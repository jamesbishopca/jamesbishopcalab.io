James Bishop Homepage
=====================

Homepage for Web Designer and Web Developer James Bishop.

Built using the Hugo static site generator and a modified version of [Creative Theme](https://github.com/digitalcraftsman/hugo-creative-theme)

Site can be seen at [jamesbishop.ca](http://jamesbishop.ca).
